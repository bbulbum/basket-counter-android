package me.kyubum.basketcounter;

import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding.view.RxView;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import me.kyubum.basketcounter.api.ApiClient;
import me.kyubum.basketcounter.attendance.AttendanceActivity;
import me.kyubum.basketcounter.dialogs.GameDialog;
import me.kyubum.basketcounter.dialogs.PlayerChangeDialog;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.models.GamePlayer;
import me.kyubum.basketcounter.models.InOut;
import me.kyubum.basketcounter.models.Player;
import me.kyubum.basketcounter.models.Stat;
import me.kyubum.basketcounter.models.StatEvent;
import me.kyubum.basketcounter.models.StatType;
import me.kyubum.basketcounter.models.Team;
import me.kyubum.basketcounter.utils.BsktRealm;
import me.kyubum.basketcounter.utils.DataLoader;
import me.kyubum.basketcounter.utils.TouchUtil;
import rx.Observable;
import rx.Subscription;

public class MainActivity extends AppCompatActivity {
    private TextView dateNoTextView, teamTextView, qtScoreTotalTextView, teamFoulTextView, plus;
    private TextView[] qtScores = new TextView[5];
    private View[] playerRows = new View[5];
    private Team team;
    private Game game;
    private RealmResults<GamePlayer> gamePlayers;
    public int currentQt = 0;
    private Realm realm;
    private GameDialog gameDialog;
    private Subscription scoreSubscription, teamFoulSubscription, inoutSubscription, advantageSubscription;
    private Subscription[] touchSubscriptions = new Subscription[5];
    private Subscription[] statSubscriptions = new Subscription[5];
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private NavigationView rightDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BsktRealm.init(this);
        realm = BsktRealm.getInstance();
        gameDialog = new GameDialog();
        initViews();

        gameDialog.setOnGameSelectListener((game, team) -> {
            this.game = game;
            this.team = team;

            gameDialog.dismiss();
            unsubscribeAll();

            AlertDialog alert = new AlertDialog.Builder(this)
                    .setTitle("데이터를 로딩중입니다.")
                    .setMessage("선수 기록 정보를 받아오는 중입니다.")
                    .setCancelable(false)
                    .create();

            alert.show();

            DataLoader.$getGameData(realm, game.id, team.id)
                .subscribe(aVoid -> {
                    initScoreBoard();
                    clearPlayerRows();
                    currentQt = -1;
                    setQuarter(0);
                    alert.dismiss();
                }, e -> {
                    Log.e("DATA FAIL", e.getMessage());
                    alert.setMessage("데이터 로딩에 실패하였습니다.");
                    alert.setCancelable(true);
                });
        });

        gameDialog.setOnCancelListener(isRefreshed -> {
            if (isRefreshed) {
                this.game = null;
                this.team = null;
                initScoreBoard();
                clearPlayerRows();
                setQuarter(0);
                gameDialog.dismiss();
            }
        });
    }

    public void initViews() {
        rightDrawer = findViewById(R.id.right_drawer);
        initDrawer();
        plus = findViewById(R.id.plus);
        dateNoTextView = findViewById(R.id.game_date_no);
        teamTextView = findViewById(R.id.game_team);
        qtScores[0] = findViewById(R.id.qt_score_1);
        qtScores[1] = findViewById(R.id.qt_score_2);
        qtScores[2] = findViewById(R.id.qt_score_3);
        qtScores[3] = findViewById(R.id.qt_score_4);
        qtScores[4] = findViewById(R.id.penalty);
        qtScoreTotalTextView = findViewById(R.id.qt_score_total);
        teamFoulTextView = findViewById(R.id.team_foul_count);

        Observable.merge(new Observable[] {
                RxView.clicks(qtScores[0]).map((v) -> 0),
                RxView.clicks(qtScores[1]).map((v) -> 1),
                RxView.clicks(qtScores[2]).map((v) -> 2),
                RxView.clicks(qtScores[3]).map((v) -> 3),
        }).subscribe(quarter -> setQuarter((int)quarter));

        RxView.clicks(qtScoreTotalTextView)
                .subscribe(view -> {
                    if (qtScores[4].getVisibility() == View.GONE) {
                        plus.setVisibility(View.VISIBLE);
                        qtScores[4].setVisibility(View.VISIBLE);
                    } else {
                        plus.setVisibility(View.GONE);
                        qtScores[4].setVisibility(View.GONE);
                    }
                });

        playerRows[0] = findViewById(R.id.player_stat_0);
        playerRows[1] = findViewById(R.id.player_stat_1);
        playerRows[2] = findViewById(R.id.player_stat_2);
        playerRows[3] = findViewById(R.id.player_stat_3);
        playerRows[4] = findViewById(R.id.player_stat_4);
        dateNoTextView.setOnClickListener(view -> gameDialog.show(getSupportFragmentManager(), "GAME_DIALOG"));
        teamTextView.setOnClickListener(view -> gameDialog.show(getSupportFragmentManager(), "GAME_DIALOG"));
    }

    public void initDrawer() {
        rightDrawer.setNavigationItemSelectedListener(item -> {
            Intent intent = new Intent(this, AttendanceActivity.class);
            switch (item.getItemId()) {
                case R.id.attendance:
                case R.id.free_throw:
                    if (this.game == null) {
                        Toast.makeText(this, "게임을 먼저 선택 해 주세요", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    int type = item.getItemId() == R.id.attendance ? AttendanceActivity.ATTENDANCE : AttendanceActivity.FREETHROW;
                    intent.putExtra("type", type);
                    intent.putExtra("date", dateFormat.format(this.game.date));
                    if (type == AttendanceActivity.FREETHROW) {
                        intent.putExtra("gameId", game.id);
                    }
                    startActivity(intent);
                    return true;
                default:
                    return false;
            }
        });
    }

    public void syncInout(RealmResults<InOut> inouts) {
        Observable.from(inouts)
                .filter(inOut -> inOut.syncedAt < inOut.updatedAt)
                .flatMap(inout -> Observable.create(subscriber -> {
                    ApiClient.postInout(inout, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            subscriber.onNext(inout);
                            subscriber.onCompleted();
                        }
                    });
                }))
                .toList()
                .subscribe(list -> {
                    realm.beginTransaction();
                    for (Object object: list) {
                        InOut inout = (InOut)object;
                        inout.syncedAt = System.currentTimeMillis();
                    }
                    realm.commitTransaction();
                });
    }

    public void inoutSubscribe() {
        Handler syncHandler = new Handler();
        AtomicReference<Runnable> runnable = new AtomicReference<>();

        if (inoutSubscription != null && !inoutSubscription.isUnsubscribed()) {
            inoutSubscription.unsubscribe();
        }

        inoutSubscription = realm.where(InOut.class)
                .equalTo("game.id", game.id)
                .equalTo("team.id", team.id)
                .equalTo("quarter", currentQt)
                .findAll()
                .asObservable()
                .subscribe(inOuts -> {
                    if (runnable.get() != null) {
                        syncHandler.removeCallbacks(runnable.get());
                    }
                    runnable.set(() -> syncInout(inOuts));
                    syncHandler.postDelayed(runnable.get(), 500);
                });
    }

    public void setQuarter(int quarter) {
        if (game == null || team == null) {
            currentQt = 0;
            qtScores[0].setSelected(false);
            qtScores[1].setSelected(false);
            qtScores[2].setSelected(false);
            qtScores[3].setSelected(false);
            return;
        }

        if (teamFoulSubscription != null && !teamFoulSubscription.isUnsubscribed()) {
            teamFoulSubscription.unsubscribe();
        }

        int prevQuarter = currentQt;

        for(int i = 0; i < qtScores.length; i++) {
            qtScores[i].setSelected(i == quarter);
        }

        currentQt = quarter;

        // inout sync subscribe
        inoutSubscribe();

        // TODO: To implement technical foul
        TouchUtil.$touchObservable(teamFoulTextView)
                .subscribe();

        teamFoulSubscription = realm.where(Stat.class)
                .equalTo("game.id", game.id)
                .equalTo("team.id", team.id)
                .equalTo("quarter", currentQt)
                .equalTo("statType", StatType.PF.id)
                .findAll()
                .asObservable()
                .subscribe((stats) -> {
                    int teamFoul = stats.sum("point").intValue();
                    teamFoulTextView.setText(Integer.toString(teamFoul));
                });

        initGamePlayers(prevQuarter);
    }

    public void playerIn(List<Player> inPlayers) {
        int gameId = game.id;
        int teamId = team.id;
        long timestamp = System.currentTimeMillis();

        HashSet<Integer> playerIds = new HashSet<>();

        for(int i = 0; i < inPlayers.size(); i++) {
            playerIds.add(inPlayers.get(i).id);
        }

        realm.executeTransactionAsync(r -> {
            r.where(InOut.class)
                    .equalTo("game.id", gameId)
                    .equalTo("team.id", teamId)
                    .equalTo("quarter", currentQt)
                    .findAll()
                    .deleteAllFromRealm();
            r.where(GamePlayer.class)
                    .equalTo("game.id", gameId)
                    .equalTo("team.id", teamId)
                    .equalTo("quarter", currentQt)
                    .findAll()
                    .deleteAllFromRealm();

            Game game = r.where(Game.class).equalTo("id", gameId).findFirst();
            Team team = r.where(Team.class).equalTo("id", teamId).findFirst();

            team.players.forEach(player -> {
                InOut inout = r.createObject(InOut.class);
                inout.game = game;
                inout.team = team;
                inout.player = player;
                inout.quarter = currentQt;
                inout.inout = playerIds.contains(player.id) ? InOut.IN : InOut.OUT;
                inout.syncedAt = 0;
                inout.updatedAt = timestamp;
            });

            RealmResults<InOut> ins = r.where(InOut.class)
                    .equalTo("game.id", game.id)
                    .equalTo("team.id", team.id)
                    .equalTo("quarter", currentQt)
                    .equalTo("inout", InOut.IN)
                    .findAllSorted("player.no", Sort.ASCENDING);

            for (int i = 0; i < 5; i++) {
                GamePlayer gamePlayer = r.createObject(GamePlayer.class);
                gamePlayer.no = i;
                gamePlayer.game = game;
                gamePlayer.team = team;
                gamePlayer.quarter = currentQt;
                gamePlayer.player = ins.get(i).player;
            }
        }, this::drawPlayerRows);
    }

    public void playerChange(Player playerOut, Player _playerIn) {
        int gameId = game.id;
        int teamId = team.id;
        int playerOutId = playerOut.id;
        int playerInId = _playerIn.id;

        realm.executeTransactionAsync(r -> {
            GamePlayer player = r.where(GamePlayer.class)
                    .equalTo("game.id", gameId)
                    .equalTo("team.id", teamId)
                    .equalTo("quarter", currentQt)
                    .equalTo("player.id", playerOutId)
                    .findFirst();
            InOut playerOutInOut = r.where(InOut.class)
                    .equalTo("game.id", gameId)
                    .equalTo("team.id", teamId)
                    .equalTo("quarter", currentQt)
                    .equalTo("player.id", playerOutId)
                    .findFirst();
            InOut playerInInOut = r.where(InOut.class)
                    .equalTo("game.id", gameId)
                    .equalTo("team.id", teamId)
                    .equalTo("quarter", currentQt)
                    .equalTo("player.id", playerInId)
                    .findFirst();

            Player playerIn = r.where(Player.class)
                    .equalTo("id", playerInId)
                    .findFirst();

            player.player = playerIn;
            playerOutInOut.inout = InOut.INOUT;
            playerOutInOut.updatedAt = System.currentTimeMillis();
            playerInInOut.inout = InOut.OUTIN;
            playerInInOut.updatedAt = System.currentTimeMillis();
        }, this::drawPlayerRows);
    }

    public void initGamePlayers(int prevQuarter) {
        AtomicBoolean needPlayerIn = new AtomicBoolean(false);
        int gameId = game.id;
        int teamId = team.id;

        realm.executeTransactionAsync(r -> {
            Game game = r.where(Game.class).equalTo("id", gameId).findFirst();
            Team team = r.where(Team.class).equalTo("id", teamId).findFirst();

            r.where(GamePlayer.class)
                    .equalTo("game.id", game.id)
                    .equalTo("team.id", team.id)
                    .equalTo("quarter", currentQt)
                    .findAll()
                    .deleteAllFromRealm();

            Integer[] leftInout = { InOut.IN, InOut.OUTIN };

            RealmResults<InOut> inouts = r.where(InOut.class)
                    .equalTo("game.id", game.id)
                    .equalTo("team.id", team.id)
                    .equalTo("quarter", currentQt)
                    .in("inout", leftInout)
                    .findAllSorted("player.no", Sort.ASCENDING);

            if (inouts.size() != 5) {
                needPlayerIn.set(true);
                return;
            }

            for (int i = 0; i < inouts.size(); i++) {
                GamePlayer gamePlayer = r.createObject(GamePlayer.class);
                InOut inout = inouts.get(i);
                gamePlayer.player = inout.player;
                gamePlayer.quarter = currentQt;
                gamePlayer.game = game;
                gamePlayer.team = team;
                gamePlayer.no = i;
            }
        }, () -> {
            if (!needPlayerIn.get() && prevQuarter != currentQt) {
                drawPlayerRows();
                return;
            }

            new PlayerChangeDialog.Builder()
                    .setGame(game)
                    .setTeam(team)
                    .setChangeType(PlayerChangeDialog.PLAYER_INIT)
                    .setOnConfirmListener(this::playerIn)
                    .setOnCancelListener(() -> {
                        if (currentQt != prevQuarter && currentQt != 0) {
                            setQuarter(prevQuarter);
                        }
                    })
                    .build()
                    .show(getSupportFragmentManager(), "PLAYER_DIALOG_INIT");
        });
    }

    public void initScoreBoard() {
        if (game == null) {
            dateNoTextView.setText("경기를 선택 해 주세요");
            teamTextView.setText("팀");
            return;
        }

        dateNoTextView.setText(dateFormat.format(game.date) + "_" + game.game + "경기");
        teamTextView.setText(team.name);

        Integer[] pointTypes = { StatType.PT3.id, StatType.PT2.id, StatType.FT.id };

        scoreSubscription = realm.where(Stat.class)
                .equalTo("game.id", game.id)
                .equalTo("team.id", team.id)
                .in("statType", pointTypes)
                .findAll()
                .asObservable()
                .subscribe(stats -> {
                    int point[] = { 0, 0, 0, 0, 0 };

                    for (Stat stat: stats) {
                        int plus = stat.player == null ? 0 : stat.player.plus;
                        int quarter = stat.quarter;
                        if (quarter < 0) quarter = 0;

                        switch (StatType.getFromId(stat.statType)) {
                            case PT3:
                                point[quarter] += (3 + plus) * stat.point;
                                break;
                            case PT2:
                                point[quarter] += (2 + plus) * stat.point;
                                break;
                            case FT:
                                point[quarter] += stat.point;
                                break;
                        }
                    }
                    int totalPoint = 0;
                    for (int i = 0; i < 5; i++) {
                        totalPoint += point[i];
                        qtScores[i].setText(Integer.toString(point[i]));
                    }

                    qtScoreTotalTextView.setText(Integer.toString(totalPoint));
                });

        advantageSubscription = realm.where(Stat.class)
                .equalTo("game.id", game.id)
                .equalTo("team.id", team.id)
                .equalTo("quarter", 4)
                .equalTo("statType", StatType.FT.id)
                .findAll()
                .asObservable()
                .filter(RealmResults::isLoaded)
                .skip(1)
                .flatMap(stats -> {
                    Log.i("post advantage", "" + stats.size());
                    int count = 0;
                    for(int i = 0; i < stats.size(); i++) {
                        count += stats.get(i).point;
                    }
                    return ApiClient.$postGameAdvantage(game.id, team.id, count);
                })
                .subscribe();
    }

    public void clearPlayerRows() {
        for (int idx = 0; idx < 5; idx++) {
            View row = playerRows[idx];

            TextView noNameText = row.findViewById(R.id.player_no_name);
            TextView pt3Text = row.findViewById(R.id.player_3pt);
            TextView pt2Text = row.findViewById(R.id.player_2pt);
            TextView ftText = row.findViewById(R.id.player_ft);
            TextView astText = row.findViewById(R.id.player_ast);
            TextView rebText = row.findViewById(R.id.player_reb);
            TextView stlText = row.findViewById(R.id.player_stl);
            TextView blkText = row.findViewById(R.id.player_blk);
            TextView toText = row.findViewById(R.id.player_to);
            TextView pfText = row.findViewById(R.id.player_pf);

            noNameText.setText("Player");
            pt3Text.setText("0");
            pt2Text.setText("0");
            ftText.setText("0");
            astText.setText("0");
            rebText.setText("0");
            stlText.setText("0");
            blkText.setText("0");
            toText.setText("0");
            pfText.setText("0");
        }
    }

    public void drawPlayerRows() {
        gamePlayers = realm.where(GamePlayer.class)
                .equalTo("game.id", game.id)
                .equalTo("team.id", team.id)
                .equalTo("quarter", currentQt)
                .findAllSorted("no", Sort.ASCENDING);

        if (gamePlayers == null || gamePlayers.size() < 5) {
            return;
        }

        for (int idx = 0; idx < Math.min(gamePlayers.size(), 5); idx++) {
            if (touchSubscriptions[idx] != null && !touchSubscriptions[idx].isUnsubscribed()) {
                touchSubscriptions[idx].unsubscribe();
            }
            if (statSubscriptions[idx] != null && !statSubscriptions[idx].isUnsubscribed()) {
                statSubscriptions[idx].unsubscribe();
            }

            View row = playerRows[idx];
            Player player = gamePlayers.get(idx).player;

            TextView noNameText = row.findViewById(R.id.player_no_name);
            TextView pt3Text = row.findViewById(R.id.player_3pt);
            TextView pt2Text = row.findViewById(R.id.player_2pt);
            TextView ftText = row.findViewById(R.id.player_ft);
            TextView astText = row.findViewById(R.id.player_ast);
            TextView rebText = row.findViewById(R.id.player_reb);
            TextView stlText = row.findViewById(R.id.player_stl);
            TextView blkText = row.findViewById(R.id.player_blk);
            TextView toText = row.findViewById(R.id.player_to);
            TextView pfText = row.findViewById(R.id.player_pf);

            noNameText.setText(player.toString());

            touchSubscriptions[idx] = Observable.merge(new Observable[] {
                    getNameTouchObservable(noNameText, player),
                    getTouchObservable(pt3Text, player),
                    getTouchObservable(pt2Text, player),
                    getTouchObservable(ftText, player),
                    getTouchObservable(astText, player),
                    getTouchObservable(rebText, player),
                    getTouchObservable(stlText, player),
                    getTouchObservable(blkText, player),
                    getTouchObservable(toText, player),
                    getTouchObservable(pfText, player),
                    getTouchObservable(qtScores[4], null, StatType.FT, 4, false)
            }).subscribe(view -> {});

            Handler syncHandler = new Handler();
            Runnable sync = () -> {
                try {
                    ApiClient.postStat(game, team, player, currentQt, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.i("SYNC STAT", "SUCCESS");
                        }
                    });
                } catch (Exception e){
                    Toast.makeText(this, "스탯정보 오류가 있습니다. 시즌을 갱신하여 서버와 동기화 해 주세요.", Toast.LENGTH_LONG).show();
                }
            };

            statSubscriptions[idx] = Observable.merge(new Observable[] {
                    getStatObservable(pt3Text, player),
                    getStatObservable(pt2Text, player),
                    getStatObservable(ftText, player),
                    getStatObservable(astText, player),
                    getStatObservable(rebText, player),
                    getStatObservable(stlText, player),
                    getStatObservable(blkText, player),
                    getStatObservable(toText, player),
                    getStatObservable(pfText, player)
            }).subscribe(view -> {
                syncHandler.removeCallbacks(sync);
                syncHandler.postDelayed(sync, 1000);
            });
        }
    }

    private void onStatEvent(StatEvent statEvent) {
        int gameId = game.id;
        int teamId = team.id;
        int playerId = statEvent.player == null ? -1 : statEvent.player.id;

        realm.executeTransactionAsync(r -> {
            Game game = r.where(Game.class).equalTo("id", gameId).findFirst();
            Team team = r.where(Team.class).equalTo("id", teamId).findFirst();
            Player player = r.where(Player.class).equalTo("id", playerId).findFirst();

            Stat stat = r.createObject(Stat.class);
            stat.game = game;
            stat.team = team;
            stat.quarter = statEvent.quarter;
            stat.player = player;
            stat.statType = statEvent.statType.id;
            stat.point = statEvent.add;
            stat.createdAt = System.currentTimeMillis();
        });
    }

    private Observable<View> getNameTouchObservable(TextView view, Player player) {
        return TouchUtil.$touchObservable(view)
                .map(type -> {
                    if (type == TouchUtil.DOUBLE_TAP) {
                        new PlayerChangeDialog.Builder()
                                .setGame(game)
                                .setTeam(team)
                                .setChangeType(PlayerChangeDialog.PLAYER_CHANGE)
                                .setOutPlayer(player)
                                .setQuarter(currentQt)
                                .setOnConfirmListener((players) -> {
                                    playerChange(player, players.get(0));
                                })
                                .build()
                                .show(getSupportFragmentManager(), "CHANGE_PLAYER_DIALOG");
                    }

                    return view;
                });
    }

    private Observable<View> getTouchObservable(final TextView view, final Player player) {
        StatType statType = StatType.getFromId(view.getId());
        return getTouchObservable(view, player, statType, currentQt, true);
    }

    private Observable<View> getTouchObservable(final TextView view, final Player player, StatType statType, int quarter, boolean isBlink) {
        return TouchUtil.$touchObservable(view)
                .doOnNext(type -> {
                    if (type == TouchUtil.SINGLE_TAP) {
                        if (isBlink) blink(view);
                        StatEvent statEvent = new StatEvent(player, statType, quarter, 1);
                        onStatEvent(statEvent);
                        return;
                    }

                    if (type == TouchUtil.DOUBLE_TAP) {
                        String[] pointStr = view.getText().toString().split(" ");
                        int point = Integer.parseInt(pointStr[0]);
                        if (point == 0) return;

                        if (isBlink) blink(view);
                        StatEvent statEvent = new StatEvent(player, statType, quarter, -1);
                        onStatEvent(statEvent);
                        return;
                    }
                })
                .map(aVoid -> view);
    }

    private void blink(View view) {
        ((TransitionDrawable)view.getBackground()).startTransition(250);
        new Handler().postDelayed(() -> ((TransitionDrawable)view.getBackground()).reverseTransition(250), 250);
    }

    private Observable<TextView> getStatObservable(final TextView textView, final Player player) {
        int statTypeId = textView.getId();
        
        RealmQuery<Stat> statQuery = realm.where(Stat.class)
                .equalTo("game.id", game.id)
                .equalTo("team.id", team.id)
                .equalTo("player.id", player.id)
                .equalTo("statType", statTypeId);

        if (statTypeId != StatType.PF.id) {
            statQuery = statQuery
                    .equalTo("quarter", currentQt);
        }

        return statQuery.findAll()
                .asObservable()
                .map(stats -> {
                    int statPoint = stats.sum("point").intValue();

                    if(statTypeId == StatType.PF.id) {
                        int quarterFoul = stats.where()
                                .equalTo("quarter", currentQt)
                                .sum("point").intValue();
                        textView.setText(quarterFoul+" ( " + statPoint + " )");
                        return textView;
                    }

                    textView.setText(Integer.toString(statPoint, 10));
                    return textView;
                })
                .skip(1);
    }

    private void unsubscribeAll() {
        if (inoutSubscription != null && !inoutSubscription.isUnsubscribed()) {
            inoutSubscription.unsubscribe();
        }

        if (scoreSubscription != null && !scoreSubscription.isUnsubscribed()) {
            scoreSubscription.unsubscribe();
        }

        if (advantageSubscription != null && !advantageSubscription.isUnsubscribed()){
            advantageSubscription.unsubscribe();
        }

        if (teamFoulSubscription != null && !teamFoulSubscription.isUnsubscribed()) {
            teamFoulSubscription.unsubscribe();
        }

        for (int idx = 0; idx < 5; idx ++) {
            if (touchSubscriptions[idx] != null && !touchSubscriptions[idx].isUnsubscribed()) {
                touchSubscriptions[idx].unsubscribe();
            }
            if (statSubscriptions[idx] != null && !statSubscriptions[idx].isUnsubscribed()) {
                statSubscriptions[idx].unsubscribe();
            }
        }
    }
}
