package me.kyubum.basketcounter.attendance;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.Date;

import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.models.Team;

public class AttendancePagerAdapter extends FragmentPagerAdapter {
    AttendanceFragment attendanceFragment;
    FreethrowFragment freethrowFragment;

    public AttendancePagerAdapter(FragmentManager fm, Date date) {
        super(fm);
        attendanceFragment = new AttendanceFragment();
        attendanceFragment.setDate(date);
        freethrowFragment = new FreethrowFragment();
    }

    public void setFreethrowGameTeam(Game game, Team team) {
        freethrowFragment.setThrowGameTeam(game, team);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return attendanceFragment;
        } else {
            return freethrowFragment;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
