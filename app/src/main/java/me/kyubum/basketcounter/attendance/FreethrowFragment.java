package me.kyubum.basketcounter.attendance;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;
import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.api.ApiClient;
import me.kyubum.basketcounter.models.Attendance;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.models.Stat;
import me.kyubum.basketcounter.models.StatType;
import me.kyubum.basketcounter.models.Team;
import me.kyubum.basketcounter.utils.BsktRealm;
import rx.Subscription;

public class FreethrowFragment extends Fragment{
    private TextView throwTeamName, throwCount;
    private RecyclerView freethrowList;
    private FreethrowListAdapter freethrowListAdapter;
    private Realm realm;
    private Subscription diffSub, throwCountSub, throwSubscription;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_freethrow, container, false);
        realm = BsktRealm.getInstance();
        throwTeamName = view.findViewById(R.id.throw_team_name);
        throwCount = view.findViewById(R.id.throw_count);
        freethrowList = view.findViewById(R.id.freethrow_list);
        freethrowListAdapter = new FreethrowListAdapter(null, null);
        freethrowList.setLayoutManager(new LinearLayoutManager(getContext()));
        freethrowList.setItemAnimator(new DefaultItemAnimator());
        freethrowList.setAdapter(freethrowListAdapter);
        return view;
    }

    @SuppressLint("DefaultLocale")
    public void setThrowGameTeam(Game game, Team team) {
        if (diffSub != null && !diffSub.isUnsubscribed()) {
            diffSub.unsubscribe();
        }

        if (throwCountSub != null && !throwCountSub.isUnsubscribed()) {
            throwCountSub.unsubscribe();
        }

        syncThrow(game, team);

        if (team == null) {
            throwTeamName.setText("자유투가 없습니다.");
            throwCount.setText("0");
            freethrowListAdapter.setGameTeam(null, null);
        } else {
            Log.i("setGameTeam", "GAMEAM");
            freethrowListAdapter.setGameTeam(game, team);

            Integer[] teamIds = { game.homeTeam.id, game.awayTeam.id };
            RealmResults<Attendance> gameAttend = realm.where(Attendance.class)
                    .equalTo("date", game.date)
                    .in("team.id", teamIds)
                    .equalTo("point", 2)
                    .findAllAsync();

            diffSub = gameAttend.asObservable().subscribe(attendances -> {
                int attend = 0, countAttend = 0;
                for(Attendance attendance: attendances) {
                    if (attendance.team.id == team.id) {
                        if (attend < 5) {
                            attend += 1;
                        }
                    } else {
                        if (countAttend < 5) {
                            countAttend += 1;
                        }
                    }
                }
                throwTeamName.setText(String.format("Game: %d | Team: %s | +%d", game.game, team.name, attend - countAttend + 1));
                freethrowListAdapter.setDiff(attend - countAttend + 1);
            });

            throwCountSub = realm.where(Stat.class)
                    .equalTo("game.id", game.id)
                    .equalTo("team.id", team.id)
                    .equalTo("statType", StatType.FT.id)
                    .equalTo("quarter", -1)
                    .findAll()
                    .asObservable()
                    .subscribe(stats -> {
                        long sum = (long)stats.sum("point");
                        throwCount.setText("+" + sum);
                    });
        }
    }

    public void syncThrow(Game game, Team team) {
        if (throwSubscription != null && !throwSubscription.isUnsubscribed()) {
            throwSubscription.unsubscribe();
        }

        if (game == null || team == null) {
            return;
        }

        Handler handler = new Handler();
        AtomicReference<Runnable> runnable = new AtomicReference<>();

        throwSubscription = realm.where(Stat.class)
                .equalTo("game.id", game.id)
                .equalTo("team.id", team.id)
                .equalTo("statType", StatType.FT.id)
                .equalTo("quarter", -1)
                .findAll()
                .asObservable()
                .subscribe(freethrows -> {
                   if (runnable.get() != null) {
                       handler.removeCallbacks(runnable.get());
                   }

                   runnable.set(() -> {
                       Map<Integer, Integer> playerFts = new HashMap<>();

                       for (Stat stat: freethrows) {
                           int sum = playerFts.getOrDefault(stat.player.id, 0);
                           sum += stat.point;
                           playerFts.put(stat.player.id, sum);
                       }

                       for (int playerId: playerFts.keySet()) {
                           ApiClient.postFreethrow(game, team, playerId, playerFts.get(playerId), new JsonHttpResponseHandler(){
                               @Override
                               public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                   Log.i("FT: "+ playerId, "SUCCESS");
                               }
                           });
                       }
                   });

                   handler.postDelayed(runnable.get(), 500);
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (throwCountSub != null && !throwCountSub.isUnsubscribed()) {
            throwCountSub.unsubscribe();
        }
        if (diffSub != null && !diffSub.isUnsubscribed()) {
            diffSub.unsubscribe();
        }
        if (throwSubscription != null && !throwSubscription.isUnsubscribed()) {
            throwSubscription.unsubscribe();
        }
    }
}
