package me.kyubum.basketcounter.attendance;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.utils.BsktRealm;
import me.kyubum.basketcounter.utils.DataLoader;
import rx.Observable;

public class AttendanceActivity extends AppCompatActivity {
    public static final int ATTENDANCE = 0, FREETHROW = 1;

    private RecyclerView attendDiffList;
    private ViewPager attendFreethrowPager;
    private LinearLayout attendanceBtn, freethrowBtn;
    private int type = ATTENDANCE;
    private Date date;
    private Game game;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = BsktRealm.getInstance();
        setContentView(R.layout.activity_attendance);
        attendDiffList = findViewById(R.id.game_team_attend_diff_list);
        attendFreethrowPager = findViewById(R.id.attendance_freethrow_viewpager);
        attendanceBtn = findViewById(R.id.attendance_nav_btn);
        freethrowBtn = findViewById(R.id.freethrow_nav_btn);
        initTypeDate();

        AlertDialog alert = new AlertDialog.Builder(this)
                .setTitle("데이터를 로딩중입니다.")
                .setMessage("출석 기록을 받아오는 중입니다.")
                .setCancelable(false)
                .create();
        alert.show();

        Observable.merge(new Observable[] {
                DataLoader.$getAttendances(realm, dateFormat.format(date)),
                DataLoader.$getFreeThrows(realm, dateFormat.format(date))
        }).last()
        .subscribe(aVoid -> {
           alert.dismiss();
           init();
        }, e -> {
            alert.setMessage("데이터 로딩에 실패하였습니다.");
            alert.setCancelable(true);
        });
    }

    public void init() {
        GameTeamDiffListAdapter gameTeamDiffListAdapter = new GameTeamDiffListAdapter(date);
        attendDiffList.setLayoutManager(new LinearLayoutManager(this));
        attendDiffList.setItemAnimator(new DefaultItemAnimator());
        attendDiffList.setAdapter(gameTeamDiffListAdapter);

        attendDiffList.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (game != null) {
                            int selectedPosition = gameTeamDiffListAdapter.setSelectedGame(game);
                            ((LinearLayoutManager)attendDiffList.getLayoutManager()).scrollToPositionWithOffset(selectedPosition, 0);
                        }
                        attendDiffList.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });

        AttendancePagerAdapter attendancePagerAdapter = new AttendancePagerAdapter(getSupportFragmentManager(), date);
        attendFreethrowPager.setOffscreenPageLimit(2);
        attendFreethrowPager.setAdapter(attendancePagerAdapter);

        gameTeamDiffListAdapter.setOnSelectListener((game, team) -> {
            this.game = game;
            if (attendFreethrowPager.getCurrentItem() != 1) {
                attendFreethrowPager.setCurrentItem(1);
            } else {
                attendancePagerAdapter.setFreethrowGameTeam(game, team);
            }
        });


        attendFreethrowPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    attendanceBtn.setSelected(true);
                    freethrowBtn.setSelected(false);
                } else {
                    attendanceBtn.setSelected(false);
                    freethrowBtn.setSelected(true);
                    if (game != null) {
                        gameTeamDiffListAdapter.setSelectedGame(game);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        attendFreethrowPager.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (type == ATTENDANCE) {
                            attendanceBtn.setSelected(true);
                            attendFreethrowPager.setCurrentItem(0);
                        } else {
                            freethrowBtn.setSelected(true);
                            attendFreethrowPager.setCurrentItem(1);
                        }
                        attendFreethrowPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });

        attendanceBtn.setOnClickListener(view -> attendFreethrowPager.setCurrentItem(0));
        freethrowBtn.setOnClickListener(view -> attendFreethrowPager.setCurrentItem(1));
    }

    public void initTypeDate() {
        Intent intent = getIntent();
        type = intent.getIntExtra("type", ATTENDANCE);
        int gameId = intent.getIntExtra("gameId", -1);
        if (gameId >= 0) {
            this.game = realm.where(Game.class).equalTo("id", gameId).findFirst();
        }
        String dateString = intent.getStringExtra("date");
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
