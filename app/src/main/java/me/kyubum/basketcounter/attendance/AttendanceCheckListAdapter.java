package me.kyubum.basketcounter.attendance;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.models.Attendance;
import me.kyubum.basketcounter.models.Player;
import me.kyubum.basketcounter.models.Team;
import me.kyubum.basketcounter.utils.BsktRealm;
import me.kyubum.basketcounter.utils.DataLoader;

public class AttendanceCheckListAdapter extends RecyclerView.Adapter{
    final int VIEW_TYPE_HEADER = 0, VIEW_TYPE_PLAYER = 1;

    private List<Item> items = new ArrayList<>();
    private RealmResults<Team> teams;
    private Date date;
    private Realm realm;
    public AttendanceCheckListAdapter(Date date) {
        realm = BsktRealm.getInstance();
        this.date = date;
        teams = realm.where(Team.class).findAll();

        for (Team team: teams) {
            items.add(new Item(team));
            RealmResults<Player> players = team.players.where().lessThan("no", 10000).findAllSorted("no", Sort.ASCENDING);
            for (Player player: players) {
                items.add(new Item(team, player));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        if (viewType == VIEW_TYPE_HEADER) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_attendance_header, parent, false);
            ViewHolderHeader viewHolderHeader = new ViewHolderHeader(itemView);
            return viewHolderHeader;
        }

        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_attendance, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        return holder;
    }

    private void attend(View view, Team team, Player player, int point, int position) {
        int playerId = player.id;
        int teamId = team.id;

        realm.executeTransactionAsync(realm -> {
            Attendance prevAttend = realm.where(Attendance.class).equalTo("datePlayer", DataLoader.dateFormat.format(date) + "_" + playerId).findFirst();

            if (prevAttend == null) {
                Attendance attendance = realm.createObject(Attendance.class, DataLoader.dateFormat.format(date) + "_" + playerId);
                attendance.date = date;
                attendance.team = realm.where(Team.class).equalTo("id", teamId).findFirst();
                attendance.player = realm.where(Player.class).equalTo("id", playerId).findFirst();
                attendance.point = point;
                attendance.updatedAt = System.currentTimeMillis();
            } else {
                prevAttend.point = view.isSelected() ? 0 : point;
                prevAttend.updatedAt = System.currentTimeMillis();
            }
        }, () -> notifyItemChanged(position));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Item item = items.get(position);
        if (item.type == VIEW_TYPE_HEADER) {
            ViewHolderHeader viewHolderHeader = (ViewHolderHeader) holder;
            viewHolderHeader.teamName.setText(item.team.name);
            return;
        }
        ViewHolder viewHolder = (ViewHolder)holder;
        viewHolder.attendanceNoName.setText(item.player.no + " " + item.player.name);

        Attendance attendance = realm.where(Attendance.class)
                .equalTo("datePlayer", DataLoader.dateFormat.format(date) + "_" + item.player.id).findFirst();

        if (attendance != null) {
            if (attendance.point == 2) {
                viewHolder.attendCheck.setSelected(true);
                viewHolder.lateCheck.setSelected(false);
            } else if (attendance.point == 1) {
                viewHolder.attendCheck.setSelected(false);
                viewHolder.lateCheck.setSelected(true);
            } else {
                viewHolder.attendCheck.setSelected(false);
                viewHolder.lateCheck.setSelected(false);
            }

            if (attendance.isLeft) {
                viewHolder.leftCheck.setSelected(true);
            } else {
                viewHolder.leftCheck.setSelected(false);
            }
        } else {
            viewHolder.attendCheck.setSelected(false);
            viewHolder.lateCheck.setSelected(false);
            viewHolder.leftCheck.setSelected(false);
        }

        viewHolder.attendCheck.setOnClickListener(view -> attend(view, item.team, item.player, 2, position));
        viewHolder.lateCheck.setOnClickListener(view -> attend(view, item.team, item.player, 1, position));
        viewHolder.leftCheck.setOnClickListener(view -> {
            if (attendance != null) {
                realm.beginTransaction();
                attendance.isLeft = !view.isSelected();
                attendance.updatedAt = System.currentTimeMillis();
                realm.commitTransaction();
                notifyItemChanged(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView attendanceNoName;
        public View attendCheck, lateCheck, leftCheck;
        public ViewHolder(View itemView) {
            super(itemView);
            attendanceNoName = itemView.findViewById(R.id.attendance_no_name);
            attendCheck = itemView.findViewById(R.id.attend_check);
            lateCheck = itemView.findViewById(R.id.late_check);
            leftCheck = itemView.findViewById(R.id.left_check);
        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        public TextView teamName;
        public ViewHolderHeader(View itemView) {
            super(itemView);
            teamName = itemView.findViewById(R.id.attendance_team_name);
        }
    }

    class Item {
        public int type;
        public Team team;
        public Player player;

        public Item(Team team) {
            this.type = VIEW_TYPE_HEADER;
            this.team = team;
        }

        public Item(Team team, Player player) {
            this.type = VIEW_TYPE_PLAYER;
            this.team = team;
            this.player = player;
        }
    }
}
