package me.kyubum.basketcounter.attendance;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.models.Player;
import me.kyubum.basketcounter.models.Stat;
import me.kyubum.basketcounter.models.StatType;
import me.kyubum.basketcounter.models.Team;
import me.kyubum.basketcounter.utils.BsktRealm;

public class FreethrowListAdapter extends RecyclerView.Adapter{
    private Game game;
    private Team team;
    private int diff = 0;
    private RealmResults<Player> players;
    private Realm realm;

    public FreethrowListAdapter(Game game, Team team) {
        realm = BsktRealm.getInstance();
        setGameTeam(game, team);
    }

    public void setGameTeam(Game game, Team team) {
        this.game = game;
        this.team = team;
        if (team != null) {
            this.players = team.players.where().lessThan("no", 10000).findAllSorted("no", Sort.ASCENDING);
        }
        notifyDataSetChanged();
    }

    public void setDiff(int diff) {
        this.diff = diff;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_freethrow, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        return holder;
    }

    public void freethrow(Player player, int position) {
        int gameId = game.id;
        int teamId = team.id;
        int playerId = player.id;

        realm.executeTransactionAsync(r -> {
            Stat stat = r.createObject(Stat.class);
            stat.statType = StatType.FT.id;
            stat.quarter = -1;
            stat.player = r.where(Player.class).equalTo("id", playerId).findFirst();
            stat.game = r.where(Game.class).equalTo("id", gameId).findFirst();
            stat.team = r.where(Team.class).equalTo("id", teamId).findFirst();
            stat.point = diff;
            stat.createdAt = new Date().getTime();
        }, () -> notifyItemChanged(position));
    }

    public void cancelFreethrow(Player player, int position) {
        int gameId = game.id;
        int teamId = team.id;
        int playerId = player.id;

        realm.executeTransactionAsync(r -> {
            Stat lastPoint = r.where(Stat.class)
                    .equalTo("game.id", gameId)
                    .equalTo("team.id", teamId)
                    .equalTo("player.id", playerId)
                    .equalTo("quarter", -1)
                    .equalTo("statType", StatType.FT.id)
                    .findAllSorted("createdAt", Sort.ASCENDING)
                    .last();
            if (lastPoint != null) {
                lastPoint.point = 0;
            }
        }, () -> notifyItemChanged(position));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder)holder;
        Player player = players.get(position);
        viewHolder.playerThrowNoName.setText(player.no + " " + player.name);

        RealmResults<Stat> fts = realm.where(Stat.class)
                .equalTo("game.id", game.id)
                .equalTo("team.id", team.id)
                .equalTo("player.id", player.id)
                .equalTo("statType", StatType.FT.id)
                .equalTo("quarter", -1)
                .greaterThan("point", 0)
                .findAll();

        long freethrowCount = fts.size();

        viewHolder.playerThrowCount.setText(Long.toString(freethrowCount, 10));

        viewHolder.minusBtn.setOnClickListener(view -> cancelFreethrow(player, position));
        viewHolder.plusBtn.setOnClickListener(view -> freethrow(player, position));
    }

    @Override
    public int getItemCount() {
        if (team == null) {
            return 0;
        }

        return players.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView playerThrowNoName, playerThrowCount, minusBtn, plusBtn;
        public ViewHolder(View itemView) {
            super(itemView);
            playerThrowNoName = itemView.findViewById(R.id.player_throw_no_name);
            playerThrowCount = itemView.findViewById(R.id.player_throw_count);
            minusBtn = itemView.findViewById(R.id.minus_btn);
            plusBtn = itemView.findViewById(R.id.plus_btn);
        }
    }
}
