package me.kyubum.basketcounter.attendance;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.models.Attendance;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.models.Team;
import me.kyubum.basketcounter.utils.BsktRealm;

public class GameTeamDiffListAdapter extends RecyclerView.Adapter{
    private Date date;
    private RealmResults<Game> games;
    private Realm realm;
    private OnSelectListener onSelectListener;
    private int selectedPosition = -1;


    public GameTeamDiffListAdapter(Date date) {
        realm = BsktRealm.getInstance();
        this.date = date;
        this.games = realm.where(Game.class).equalTo("date", date).findAll().sort("game", Sort.ASCENDING);
        realm.where(Attendance.class).equalTo("date", date).equalTo("point", 2).findAll().asObservable().subscribe(attendances -> {
            notifyDataSetChanged();
        });
    }

    public int setSelectedGame(Game selectedGame) {
        int selectedId = selectedGame.id;
        for(int i = 0; i < games.size(); i++) {
            Game game = games.get(i);
            if (game.id == selectedId) {
                long homeAttendance = realm.where(Attendance.class)
                        .equalTo("date", date).equalTo("team.id", game.homeTeam.id).equalTo("point", 2).count();
                long awayAttendance = realm.where(Attendance.class)
                        .equalTo("date", date).equalTo("team.id", game.awayTeam.id).equalTo("point", 2).count();

                Team selectedTeam;
                if (homeAttendance >= 5 && awayAttendance >= 5 || homeAttendance == awayAttendance) {
                    selectedTeam = null;
                } else if (homeAttendance > awayAttendance) {
                    selectedTeam = game.homeTeam;
                } else {
                    selectedTeam = game.awayTeam;
                }

                onSelectListener.onSelect(game, selectedTeam);
                selectedPosition = i;
                notifyItemChanged(i);
                return i;
            }
        }
        return 0;
    }

    public void setOnSelectListener(OnSelectListener onSelectListener) {
        this.onSelectListener = onSelectListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_game_team_attend_diff, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder)holder;

        Game game = games.get(position);
        long homeAttendance = realm.where(Attendance.class)
                .equalTo("date", date).equalTo("team.id", game.homeTeam.id).equalTo("point", 2).count();
        long awayAttendance = realm.where(Attendance.class)
                .equalTo("date", date).equalTo("team.id", game.awayTeam.id).equalTo("point", 2).count();

        Team selectedTeam;
        if (homeAttendance >= 5 && awayAttendance >= 5 || homeAttendance == awayAttendance) {
            selectedTeam = null;
        } else if (homeAttendance > awayAttendance) {
            selectedTeam = game.homeTeam;
        } else {
            selectedTeam = game.awayTeam;
        }

        if (position == selectedPosition) {
            viewHolder.itemView.setSelected(true);
        } else {
            viewHolder.itemView.setSelected(false);
        }

        viewHolder.homeTeamName.setText(game.homeTeam.name);
        viewHolder.homeTeamAttendance.setText(Long.toString(homeAttendance, 10));
        viewHolder.awayTeamName.setText(game.awayTeam.name);
        viewHolder.awayTeamAttendance.setText(Long.toString(awayAttendance, 10));

        viewHolder.itemView.setOnClickListener(view -> {
            int prevPosition = selectedPosition;
            selectedPosition = position;
            if (prevPosition >= 0) {
                notifyItemChanged(prevPosition);
            }
            notifyItemChanged(position);
            onSelectListener.onSelect(game, selectedTeam);
        });
    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public TextView homeTeamName, homeTeamAttendance, awayTeamName, awayTeamAttendance;
        public ViewHolder(View itemView) {
            super(itemView);
            homeTeamName = itemView.findViewById(R.id.home_team_name);
            homeTeamAttendance = itemView.findViewById(R.id.home_team_attendance);
            awayTeamName = itemView.findViewById(R.id.away_team_name);
            awayTeamAttendance = itemView.findViewById(R.id.away_team_attendance);
        }
    }

    public interface OnSelectListener {
        void onSelect(Game game, Team team);
    }
}
