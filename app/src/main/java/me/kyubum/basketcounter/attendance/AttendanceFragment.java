package me.kyubum.basketcounter.attendance;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.api.ApiClient;
import me.kyubum.basketcounter.models.Attendance;
import me.kyubum.basketcounter.models.Season;
import me.kyubum.basketcounter.utils.BsktRealm;
import rx.Subscription;

public class AttendanceFragment extends Fragment{
    private RecyclerView attendanceList;
    private Date date;
    private Subscription attendSub;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attendance, container, false);
        attendanceList = view.findViewById(R.id.attendance_list);
        AttendanceCheckListAdapter attendanceCheckListAdapter = new AttendanceCheckListAdapter(date);
        attendanceList.setLayoutManager(new LinearLayoutManager(getContext()));
        attendanceList.setItemAnimator(new DefaultItemAnimator());
        attendanceList.setAdapter(attendanceCheckListAdapter);
        syncAttendance();
        return view;
    }

    public void syncAttendance() {
        if (attendSub != null && !attendSub.isUnsubscribed()) {
            attendSub.unsubscribe();
        }

        Handler handler = new Handler();
        AtomicReference<Runnable> runnable = new AtomicReference<>();

        Realm realm = BsktRealm.getInstance();
        Season season = realm.where(Season.class).equalTo("isSelected", true).findFirst();

        attendSub = realm.where(Attendance.class)
                .equalTo("date", date)
                .findAll()
                .asObservable()
                .map(attendances -> attendances
                        .stream()
                        .filter(attendance -> attendance.syncedAt < attendance.updatedAt)
                        .collect(Collectors.toList())
                )
                .subscribe(attendances -> {
                    if (runnable.get() != null) {
                        handler.removeCallbacks(runnable.get());
                    }

                    runnable.set(() -> {
                        for (Attendance attendance: attendances) {
                            ApiClient.postAttendance(season, attendance, new JsonHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                    Log.i("Attendance :" + attendance.player.id, "SUCCESS");
                                    realm.beginTransaction();
                                    attendance.syncedAt = System.currentTimeMillis();
                                    realm.commitTransaction();
                                }
                            });
                        }
                    });

                    handler.postDelayed(runnable.get(), 500);
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (attendSub != null && !attendSub.isUnsubscribed()) {
            attendSub.unsubscribe();
        }
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
