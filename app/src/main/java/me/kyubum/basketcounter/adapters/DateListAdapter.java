package me.kyubum.basketcounter.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.utils.BsktRealm;

public class DateListAdapter extends RecyclerView.Adapter {
    private Realm realm;
    private RealmResults<Game> dates;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private DateSelectListener dateSelectListener;
    private int prevSelectedPosition = RecyclerView.NO_POSITION;
    public int selectedPosition = RecyclerView.NO_POSITION;

    public DateListAdapter(DateSelectListener dateSelectListener) {
        realm = BsktRealm.getInstance();
        dates = realm.where(Game.class).distinct("date").sort("date");
        this.dateSelectListener = dateSelectListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_game_date, parent, false);
        return new DateViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder _holder, int position) {
        DateViewHolder holder = (DateViewHolder)_holder;
        Date date = dates.get(position).date;
        holder.dateText.setText(dateFormat.format(date));
        holder.itemView.setSelected(selectedPosition == position);
    }

    @Override
    public int getItemCount() {
        if (dates == null) return 0;
        return dates.size();
    }

    public interface DateSelectListener {
        void onSelect(int position, Date date);
    }

    class DateViewHolder extends RecyclerView.ViewHolder {
        public TextView dateText;

        public DateViewHolder(View view) {
            super(view);
            dateText = view.findViewById(R.id.row_date_text);

            itemView.setOnClickListener((v) -> {
                int position = getAdapterPosition();
                Date date = dates.get(position).date;
                dateSelectListener.onSelect(position, date);

                prevSelectedPosition = selectedPosition;
                selectedPosition = position;
                notifyItemChanged(prevSelectedPosition);
                notifyItemChanged(selectedPosition);
            });
        }
    }
}
