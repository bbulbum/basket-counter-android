package me.kyubum.basketcounter.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.dialogs.PlayerChangeDialog;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.models.GamePlayer;
import me.kyubum.basketcounter.models.InOut;
import me.kyubum.basketcounter.models.Player;
import me.kyubum.basketcounter.models.Team;
import me.kyubum.basketcounter.utils.BsktRealm;

public class PlayerChangeAdapter extends RecyclerView.Adapter {
    private List<PlayerInout> players = new ArrayList<>();
    public SparseArray<Player> selectedPositions = new SparseArray<>();
    private int changeType;
    private int quarter = -1;

    public PlayerChangeAdapter(Game game, Team team, int changeType) {
        this(game, team, changeType, -1);
    }

    public PlayerChangeAdapter(Game game, Team team, int changeType, int quarter) {
        this.changeType = changeType;
        this.quarter = quarter;

        Realm realm = BsktRealm.getInstance();

        RealmResults<Player> benchs = team.players.sort("no", Sort.ASCENDING);

        if (changeType == PlayerChangeDialog.PLAYER_CHANGE) {
            RealmResults<GamePlayer> curPlayers = realm.where(GamePlayer.class)
                    .equalTo("game.id", game.id)
                    .equalTo("team.id", team.id)
                    .equalTo("quarter", quarter)
                    .findAll();
            if (curPlayers.size() > 0) {
                Integer[] curPlayerNo = new Integer[curPlayers.size()];
                for (int i = 0; i < curPlayerNo.length; i++) {
                    curPlayerNo[i] = curPlayers.get(i).player.no;
                }
                benchs = team.players.where()
                        .not()
                        .in("no", curPlayerNo)
                        .findAllSorted("no", Sort.ASCENDING);
            }
        }

        for(Player player: benchs) {
            PlayerInout pi = new PlayerInout();
            pi.player = player;
            RealmResults<InOut> inouts = realm.where(InOut.class)
                    .equalTo("game.id", game.id)
                    .equalTo("team.id", team.id)
                    .equalTo("player.no", player.no)
                    .findAllSorted("quarter", Sort.ASCENDING);
            for(InOut inout: inouts) {
                pi.inouts[inout.quarter] = inout.inout;
            }
            players.add(pi);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_player_change, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder)holder;
        PlayerInout pi = players.get(position);
        viewHolder.playerNoName.setText(pi.player.toString());
        for (int quarter = 0; quarter < 4; quarter++) {
            double inout = pi.inouts[quarter];
            if (inout == InOut.OUT) {
                viewHolder.qtInouts[quarter].setSelected(false);
                viewHolder.qtInouts[quarter].setImageAlpha(0);
            } else if (inout == InOut.IN) {
                viewHolder.qtInouts[quarter].setSelected(true);
                viewHolder.qtInouts[quarter].setImageAlpha(255);
            } else {
                viewHolder.qtInouts[quarter].setSelected(false);
                viewHolder.qtInouts[quarter].setImageAlpha(255);
            }
        }
        viewHolder.playerNoName.setSelected(selectedPositions.get(position) != null);
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    class PlayerInout {
        public Player player;
        public int[] inouts = { InOut.OUT, InOut.OUT, InOut.OUT, InOut.OUT };
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView[] qtInouts = new ImageView[4];
        public TextView playerNoName;

        public ViewHolder(View view) {
            super(view);
            qtInouts[0] = view.findViewById(R.id.qt_inout_1);
            qtInouts[1] = view.findViewById(R.id.qt_inout_2);
            qtInouts[2] = view.findViewById(R.id.qt_inout_3);
            qtInouts[3] = view.findViewById(R.id.qt_inout_4);
            playerNoName = view.findViewById(R.id.player_no_name);

            playerNoName.setOnClickListener((v) -> {
                int position = getAdapterPosition();

                if (selectedPositions.get(position) != null) {
                    selectedPositions.delete(position);
                } else {
                    if (changeType == PlayerChangeDialog.PLAYER_CHANGE) {
                        selectedPositions.clear();
                    }

                    if (selectedPositions.size() < 5){
                        selectedPositions.put(position, players.get(position).player);
                    }
                }

                notifyDataSetChanged();
            });
        }
    }
}
