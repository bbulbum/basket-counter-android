package me.kyubum.basketcounter.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.dialogs.GameDialog;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.utils.BsktRealm;

public class GameListAdapter extends Adapter{
    private Realm realm;
    private RealmResults<Game> games;
    private GameDialog.OnGameSelectListener onGameSelectListener;

    public GameListAdapter(GameDialog.OnGameSelectListener onGameSelectListener) {;
        realm = BsktRealm.getInstance();
        games = realm.where(Game.class).equalTo("id", 0).findAll();
        this.onGameSelectListener = onGameSelectListener;
    }

    public void setDate(Date date) {
        games = realm.where(Game.class).equalTo("date", date).findAll();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_game_team, parent, false);
        return new GameListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder _holder, int position) {
        GameListViewHolder holder = (GameListViewHolder) _holder;
        Game game = games.get(position);
        holder.no.setText(Integer.toString(game.game));
        holder.homeTeam.setText(game.homeTeam.name);
        holder.awayTeam.setText(game.awayTeam.name);
    }

    @Override
    public int getItemCount() {
        if (games == null) return 0;
        return games.size();
    }

    class GameListViewHolder extends RecyclerView.ViewHolder{
        public TextView no, homeTeam, awayTeam;

        public GameListViewHolder(View view) {
            super(view);
            no = view.findViewById(R.id.row_game_number);
            homeTeam = view.findViewById(R.id.row_home_team);
            awayTeam = view.findViewById(R.id.row_away_team);

            homeTeam.setOnClickListener((v) -> {
                Game game = games.get(getAdapterPosition());
                onGameSelectListener.onSelect(game, game.homeTeam);
            });

            awayTeam.setOnClickListener((v) -> {
                Game game = games.get(getAdapterPosition());
                onGameSelectListener.onSelect(game, game.awayTeam);
            });
        }
    }
}
