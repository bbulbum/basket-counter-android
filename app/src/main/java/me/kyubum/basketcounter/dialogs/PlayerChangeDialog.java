package me.kyubum.basketcounter.dialogs;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.adapters.PlayerChangeAdapter;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.models.Player;
import me.kyubum.basketcounter.models.Team;
import rx.Observable;
import rx.Subscription;

public class PlayerChangeDialog extends DialogFragment {
    public static final int PLAYER_INIT = 0;
    public static final int PLAYER_CHANGE = 1;

    private RecyclerView playerList;
    private TextView cancelChange, confirmChange;
    private Game game;
    private Team team;
    private int changeType = PLAYER_INIT;
    private int quarter = -1;
    private Subscription clickSubscription;
    private OnConfirmListener onConfirmListener;
    private OnCancelListener onCancelListener;
    private PlayerChangeAdapter playerChangeAdapter;
    private Player outPlayer;

    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        this.onConfirmListener = onConfirmListener;
    }

    public void setOnCancelListener(OnCancelListener onCancelListener) {
        this.onCancelListener = onCancelListener;
    }

    public void initData(Game game, Team team, int type) {
        this.game = game;
        this.team = team;
        this.changeType = type;
    }

    public void setQuarter(int quarter) { this.quarter = quarter; }
    public void setOutPlayer(Player outPlayer) {
        this.outPlayer = outPlayer;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_player_change, container, false);
        playerList = view.findViewById(R.id.player_list);
        cancelChange = view.findViewById(R.id.cancel_change);
        confirmChange = view.findViewById(R.id.confirm_change);

        playerList.setLayoutManager(new LinearLayoutManager(getContext()));
        playerList.setItemAnimator(new DefaultItemAnimator());

        return view;
    }

    @Override
    public void onResume() {
        playerChangeAdapter = new PlayerChangeAdapter(game, team, changeType, quarter);
        playerList.setAdapter(playerChangeAdapter);
        playerChangeAdapter.notifyDataSetChanged();

        clickSubscription = Observable.merge(new Observable[]{
                RxView.clicks(cancelChange).map(event -> {
                    if (onCancelListener != null) {
                        onCancelListener.onCancel();
                    }
                    dismiss();
                    return event;
                }),
                RxView.clicks(confirmChange).map(event -> {
                    List<Player> players = new ArrayList<>();
                    for (int i = 0; i < playerChangeAdapter.selectedPositions.size(); i++) {
                        int key = playerChangeAdapter.selectedPositions.keyAt(i);
                        players.add(playerChangeAdapter.selectedPositions.get(key));
                    }
                    if (changeType == PLAYER_INIT && players.size() < 5) {
                        Toast.makeText(getContext(), "5명을 전부 선택 해 주세요", Toast.LENGTH_SHORT).show();
                        return event;
                    }
                    if (changeType == PLAYER_CHANGE) {
                        if (players.size() == 0) {
                            Toast.makeText(getContext(), "교체할 선수를 선택 해 주세요", Toast.LENGTH_SHORT).show();
                            return event;
                        }
                        if (outPlayer != null && players.get(0).no == outPlayer.no) {
                            Toast.makeText(getContext(), "같은 선수로 교체할 수 없습니다.", Toast.LENGTH_SHORT).show();
                            return event;
                        }
                    }
                    onConfirmListener.onConfirm(players);
                    dismiss();
                    return event;
                })
        }).subscribe();

        super.onResume();
    }

    @Override
    public void onDestroyView() {
        clickSubscription.unsubscribe();
        super.onDestroyView();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        if (onCancelListener != null) onCancelListener.onCancel();
    }

    public interface OnConfirmListener {
        void onConfirm(List<Player> players);
    }

    public interface OnCancelListener {
        void onCancel();
    }

    public static class Builder {
        private Game game;
        private Team team;
        private int changeType;
        private int quarter = -1;
        private Player outPlayer;
        private OnConfirmListener onConfirmListener;
        private OnCancelListener onCancelListener;

        public Builder setGame(Game game) {
            this.game = game;
            return this;
        }

        public Builder setTeam(Team team) {
            this.team = team;
            return this;
        }

        public Builder setChangeType(int changeType) {
            this.changeType = changeType;
            return this;
        }

        public Builder setOnConfirmListener(OnConfirmListener onConfirmListener) {
            this.onConfirmListener = onConfirmListener;
            return this;
        }

        public Builder setOnCancelListener(OnCancelListener onCancelListener) {
            this.onCancelListener = onCancelListener;
            return this;
        }

        public Builder setOutPlayer(Player outPlayer) {
            this.outPlayer = outPlayer;
            return this;
        }

        public Builder setQuarter(int quarter) {
            this.quarter = quarter;
            return this;
        }

        public PlayerChangeDialog build() {
            PlayerChangeDialog dialog = new PlayerChangeDialog();
            dialog.initData(game, team, changeType);
            dialog.setOnConfirmListener(onConfirmListener);
            dialog.setOnCancelListener(onCancelListener);
            dialog.setOutPlayer(outPlayer);
            dialog.setQuarter(quarter);
            return dialog;
        }
    }
}
