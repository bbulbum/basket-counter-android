package me.kyubum.basketcounter.dialogs;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.jakewharton.rxbinding.widget.RxAdapterView;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;
import me.kyubum.basketcounter.R;
import me.kyubum.basketcounter.adapters.DateListAdapter;
import me.kyubum.basketcounter.adapters.GameListAdapter;
import me.kyubum.basketcounter.api.ApiClient;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.models.Season;
import me.kyubum.basketcounter.models.Team;
import me.kyubum.basketcounter.utils.BsktRealm;
import me.kyubum.basketcounter.utils.DataLoader;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

public class GameDialog extends DialogFragment{
    private Realm realm;
    private Spinner seasonSelect;
    private ImageButton seasonRefresh;
    private RecyclerView gameDateListView, gameGameListView;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private OnGameSelectListener onGameSelectListener;
    private OnCancelListener onCancelListener;
    private ArrayAdapter seasonAdapter;
    private GameListAdapter gameListAdapter;
    private DateListAdapter dateListAdapter;
    private int selectedPosition;
    private Date date;
    private boolean isRefresh = false;

    public GameDialog() {
        realm = BsktRealm.getInstance();
    }

    public void setOnGameSelectListener(OnGameSelectListener onGameSelectListener) {
        this.onGameSelectListener = onGameSelectListener;
    }

    public void setOnCancelListener(OnCancelListener onCancelListener) {
        this.onCancelListener = onCancelListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_game_select, container);
        seasonSelect = view.findViewById(R.id.season_select);
        seasonRefresh = view.findViewById(R.id.season_refresh);
        gameDateListView = view.findViewById(R.id.game_dates);
        gameGameListView = view.findViewById(R.id.game_games);

        gameGameListView.setLayoutManager(new LinearLayoutManager(getContext()));
        gameGameListView.setItemAnimator(new DefaultItemAnimator());
        gameListAdapter = new GameListAdapter(onGameSelectListener);
        gameGameListView.setAdapter(gameListAdapter);

        gameDateListView.setLayoutManager(new LinearLayoutManager(getContext()));
        gameDateListView.setItemAnimator(new DefaultItemAnimator());
        DateListAdapter.DateSelectListener dateSelectListener = (position, date) -> {
            ((LinearLayoutManager)gameDateListView.getLayoutManager()).scrollToPositionWithOffset(position, 0);
            gameListAdapter.setDate(date);
            this.selectedPosition = position;
            this.date = date;
        };
        dateListAdapter = new DateListAdapter(dateSelectListener);
        gameDateListView.setAdapter(dateListAdapter);

        setSeasonSpinner();
        seasonRefresh.setOnClickListener((v) -> {
            AlertDialog alert = new AlertDialog.Builder(getContext())
                    .setMessage("시즌 목록을 받아오는 중입니다.")
                    .create();
            alert.show();

            getSeasons(() -> {
                alert.dismiss();
                setSeasonSpinner();
            });
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        isRefresh = false;
        if (date != null) {
            dateListAdapter.selectedPosition = selectedPosition;
            dateListAdapter.notifyItemChanged(selectedPosition);
            gameListAdapter.setDate(date);
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        onCancelListener.onCancel(isRefresh);
    }

    private void setSeasonSpinner() {
        String[] seasonStrings = getSeasonList();
        seasonAdapter = new ArrayAdapter(getContext(), R.layout.row_season, seasonStrings);
        seasonAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        seasonSelect.setAdapter(seasonAdapter);

        AlertDialog alert = new AlertDialog.Builder(getContext())
                .setTitle("데이터를 로딩중입니다.")
                .setMessage("팀, 플레이어 정보를 받아오는 중입니다.")
                .setCancelable(false)
                .create();

        RxAdapterView.itemSelections(seasonSelect)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(position -> {
                    if (position == 0) {
                        return;
                    }

                    Season season = realm.where(Season.class).equalTo("season", seasonStrings[position]).findFirst();
                    if (season == null) {
                        return;
                    }
                    isRefresh = true;

                    realm.beginTransaction();
                    Season curSeason = realm.where(Season.class).equalTo("isSelected", true).findFirst();
                    if (curSeason != null) {
                        curSeason.isSelected = false;
                    }
                    season.isSelected = true;
                    realm.commitTransaction();

                    alert.show();

                    DataLoader.$getTeamMembers(realm, season.id)
                            .flatMap(aVoid -> {
                                alert.setMessage("라운드, 게임 정보를 받아오는 중입니다.");
                                return DataLoader.$getGames(realm, season.id);
                            })
                            .subscribe(aVoid -> {
                                dateListAdapter.notifyDataSetChanged();
                                alert.dismiss();
                            }, e -> {
                                alert.setCancelable(true);
                                alert.setMessage("데이터 로딩에 실패하였습니다.");
                            });

                    setSeasonSpinner();
                }, e -> {
                    alert.setCancelable(true);
                    alert.setMessage("데이터 로딩에 실패하였습니다.");
                });
    }

    private String[] getSeasonList() {
        RealmResults<Season> seasons = realm.where(Season.class).findAll().sort("season");
        String[] seasonArray = new String[seasons.size() + 1];
        seasonArray[0] = "시즌을 선택 해 주세요";
        for (int i = 0; i < seasons.size(); i++) {
            Season season = seasons.get(i);
            seasonArray[i + 1] = season.season;
            if (season.isSelected) {
                seasonArray[0] = season.season;
            }
        }
        return seasonArray;
    }

    private void getSeasons(DataLoader.OnFinished onFinished) {
        Season selectedSeason = realm.where(Season.class).equalTo("isSelected", true).findFirst();
        int selectedSeasonId = selectedSeason != null ? selectedSeason.id : 0;

        ApiClient.getSeasons(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    realm.beginTransaction();
                    realm.delete(Season.class);
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject object = response.getJSONObject(i);
                        Season season = realm.createObject(Season.class, object.getInt("id"));
                        season.season = object.getString("season");
                        if (season.id == selectedSeasonId) {
                            season.isSelected = true;
                        }
                    }
                    realm.commitTransaction();
                    onFinished.onFinish();
                } catch (JSONException e) {
                    realm.cancelTransaction();
                    e.printStackTrace();
                }
            }
        });
    }

    public interface OnGameSelectListener {
        void onSelect(Game game, Team team);
    }

    public interface OnCancelListener {
        void onCancel(boolean isRefreshed);
    }
}
