package me.kyubum.basketcounter.models;

import io.realm.RealmObject;

public class GamePlayer extends RealmObject {
    public Game game;
    public Team team;
    public int no = 0;
    public int quarter = 0;
    public Player player;
}
