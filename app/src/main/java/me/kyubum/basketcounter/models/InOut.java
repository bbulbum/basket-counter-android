package me.kyubum.basketcounter.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class InOut extends RealmObject {
    public static int OUT = 1, IN = 2, INOUT = 3, OUTIN = 4;

    public Game game;
    public Team team;
    public int quarter = 0;
    public Player player;
    public int inout = OUT;

    public long syncedAt;
    public long updatedAt;

    public static InOut build(Player player) {
        InOut inout = new InOut();
        inout.player = player;
        return inout;
    }
}
