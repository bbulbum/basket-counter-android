package me.kyubum.basketcounter.models;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Attendance extends RealmObject {
    @PrimaryKey
    public String datePlayer;
    public Date date;
    public Team team;
    public Player player;
    public int point;
    public boolean isLeft = false;

    public long syncedAt;
    public long updatedAt;
}
