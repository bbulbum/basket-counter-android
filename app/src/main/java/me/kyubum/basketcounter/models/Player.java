package me.kyubum.basketcounter.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Player extends RealmObject{
    @PrimaryKey
    public int id;

    public int no;
    public String name;
    public int plus = 0;

    public String toString() {
        if (no > 1000) {
            return name;
        }
        return this.no + " " + this.name;
    }
}
