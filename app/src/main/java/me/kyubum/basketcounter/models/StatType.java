package me.kyubum.basketcounter.models;

import me.kyubum.basketcounter.R;

public enum StatType {
    PT3(R.id.player_3pt, "PT3"),PT2(R.id.player_2pt, "PT2"),FT(R.id.player_ft, "FT"),
    AST(R.id.player_ast, "AST"),REB(R.id.player_reb, "REB"),STL(R.id.player_stl, "STL"),
    BLK(R.id.player_blk, "BLK"),TO(R.id.player_to, "TO"),PF(R.id.player_pf, "PF");

    public int id;
    public String type;

    StatType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    static public StatType getFromString(String _type) {
        String type = _type.toUpperCase();

        for (StatType statType: StatType.values()) {
            if (type.equals(statType.type)) return statType;
        }

        return null;
    }

    static public StatType getFromId(int id) {
        for (StatType statType: StatType.values()) {
            if (id == statType.id) return statType;
        }

        return null;
    }
}
