package me.kyubum.basketcounter.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Team extends RealmObject {
    @PrimaryKey
    public int id;
    public String name;
    public RealmList<Player> players;
}
