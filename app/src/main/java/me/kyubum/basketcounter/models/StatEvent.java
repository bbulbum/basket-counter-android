package me.kyubum.basketcounter.models;

public class StatEvent {
    public Player player;
    public StatType statType;
    public int quarter;
    public int add;

    public StatEvent(Player player, StatType statType, int quarter, int add) {
        this.player = player;
        this.statType = statType;
        this.quarter = quarter;
        this.add = add;
    }
}
