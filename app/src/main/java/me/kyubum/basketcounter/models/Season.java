package me.kyubum.basketcounter.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Season extends RealmObject {
    @PrimaryKey
    public int id;
    public boolean isSelected = false;
    public String season;
}
