package me.kyubum.basketcounter.models;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class Game extends RealmObject{
    @PrimaryKey
    public int id;
    @Index
    public Date date;
    public int game;
    public Team homeTeam, awayTeam;
    public int homePlus, awayPlus;
}
