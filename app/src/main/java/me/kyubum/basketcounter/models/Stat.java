package me.kyubum.basketcounter.models;

import io.realm.RealmObject;
import io.realm.annotations.Index;

public class Stat extends RealmObject {
    public Game game;
    public Team team;
    public Player player;
    public int quarter;

    @Index
    public int statType;
    public int point;

    public long createdAt;
}
