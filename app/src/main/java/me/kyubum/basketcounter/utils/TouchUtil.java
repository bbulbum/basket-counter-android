package me.kyubum.basketcounter.utils;

import android.support.v4.view.GestureDetectorCompat;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import rx.Observable;
import rx.Subscriber;

public class TouchUtil {
    public static final int SINGLE_TAP = 0;
    public static final int DOUBLE_TAP = 1;

    public static Observable<Integer> $touchObservable(View view) {
        return Observable.create((Subscriber<? super Integer> subscriber) -> {
            GestureDetectorCompat gestureDetector = new GestureDetectorCompat(view.getContext(), new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDown(MotionEvent e) {
                    return true;
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    subscriber.onNext(DOUBLE_TAP);
                    return super.onDoubleTap(e);
                }

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    subscriber.onNext(SINGLE_TAP);
                    return super.onSingleTapConfirmed(e);
                }
            });

            view.setOnTouchListener((View view1, MotionEvent motionEvent) -> gestureDetector.onTouchEvent(motionEvent));
        });
    }
}

