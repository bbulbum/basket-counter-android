package me.kyubum.basketcounter.utils;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BsktRealm {
    private static Realm realm;

    public static void init(Context context) {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(config);
    }

    public static Realm getInstance() {
        return realm;
    }
}
