package me.kyubum.basketcounter.utils;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;
import me.kyubum.basketcounter.api.ApiClient;
import me.kyubum.basketcounter.models.Attendance;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.models.InOut;
import me.kyubum.basketcounter.models.Player;
import me.kyubum.basketcounter.models.Stat;
import me.kyubum.basketcounter.models.StatType;
import me.kyubum.basketcounter.models.Team;
import rx.Observable;
import rx.Subscriber;
import rx.observables.ConnectableObservable;

public class DataLoader {
    static public SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    static public Observable<Void> $getTeamMembers(Realm realm, int seasonId) {
        return Observable.create(subscriber -> ApiClient.getTeamMembers(seasonId, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    realm.beginTransaction();
                    realm.where(Stat.class).findAll().deleteAllFromRealm();
                    realm.where(Attendance.class).findAll().deleteAllFromRealm();
                    realm.where(InOut.class).findAll().deleteAllFromRealm();
                    realm.where(Team.class).findAll().deleteAllFromRealm();
                    realm.where(Player.class).findAll().deleteAllFromRealm();

                    Player invite1 = realm.createObject(Player.class, 99998);
                    invite1.no = 99998;
                    invite1.name = "용병 1";
                    invite1.plus = 0;

                    Player invite2 = realm.createObject(Player.class, 99999);
                    invite2.no = 99999;
                    invite2.name = "용병 2";
                    invite2.plus = 0;


                    for (int i = 0; i < response.length(); i++) {
                        JSONObject teamObj = response.getJSONObject(i);
                        Team team = realm.createObject(Team.class, teamObj.getInt("id"));
                        team.name = teamObj.getString("name");
                        JSONArray membersArray = teamObj.getJSONArray("members");
                        for (int j = 0; j < membersArray.length(); j++) {
                            JSONObject memberObj = membersArray.getJSONObject(j);
                            Player player = realm.createObject(Player.class, memberObj.getInt(("id")));
                            player.no = memberObj.getInt("no");
                            player.name = memberObj.getString("name");
                            player.plus = memberObj.getInt("plus");
                            team.players.add(player);
                        }
                        team.players.add(invite1);
                        team.players.add(invite2);
                    }

                    realm.commitTransaction();
                    subscriber.onNext(null);
                    subscriber.onCompleted();
                } catch (JSONException e) {
                    e.printStackTrace();
                    realm.cancelTransaction();
                    subscriber.onError(e);
                }
            }
        }));
    }

    static public Observable<Void> $getGameData(Realm realm, int gameId, int teamId) {
        return Observable.merge(new Observable[]{
                $getStats(realm, gameId, teamId),
                $getInouts(realm, gameId, teamId)
        }).last();
    }

    static public Observable<Void> $getFreeThrows(Realm realm, String gameDate) {
        return ApiClient.$getFreeThrows(gameDate)
                .map(response -> {
                    long timestamp = System.currentTimeMillis();

                    realm.beginTransaction();
                    try {
                        realm.where(Stat.class)
                                .equalTo("game.date", dateFormat.parse(gameDate))
                                .equalTo("quarter", -1)
                                .findAll()
                                .deleteAllFromRealm();

                        for (int i = 0; i < response.length(); i++) {
                            JSONObject object = response.getJSONObject(i);
                            Stat stat = realm.createObject(Stat.class);
                            stat.player = realm.where(Player.class).equalTo("id", object.getInt("memberId")).findFirst();
                            stat.game = realm.where(Game.class).equalTo("id", object.getInt("gameId")).findFirst();
                            stat.team = realm.where(Team.class).equalTo("id", object.getInt("teamId")).findFirst();
                            stat.point = object.getInt("ft");
                            stat.quarter = -1;
                            stat.statType = StatType.FT.id;
                            stat.createdAt = timestamp;
                        }
                        realm.commitTransaction();
                    } catch (JSONException | ParseException e) {
                        e.printStackTrace();
                        realm.cancelTransaction();
                        throw new Error(e.getMessage());
                    }
                    return null;
                });
    }

    static public Observable<Void> $getAttendances(Realm realm, String gameDate) {
        return Observable.create(subscriber -> ApiClient.getAttendances(gameDate, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                realm.beginTransaction();
                realm.where(Attendance.class).findAll().deleteAllFromRealm();
                long timestamp = System.currentTimeMillis();

                try {
                    for(int i = 0 ; i < response.length(); i++) {
                        JSONObject object = response.getJSONObject(i);
                        String gameDate = object.getString("gameDate");
                        int playerId = object.getInt("memberId");
                        int teamId = object.getInt("teamId");
                        int point = object.getInt("point");
                        boolean isLeft = object.getBoolean("isLeft");
                        Attendance attendance = realm.createObject(Attendance.class, gameDate + "_" + playerId);
                        attendance.date = dateFormat.parse(gameDate);
                        attendance.player = realm.where(Player.class).equalTo("id", playerId).findFirst();
                        attendance.team = realm.where(Team.class).equalTo("id", teamId).findFirst();
                        attendance.point = point;
                        attendance.isLeft = isLeft;
                        attendance.syncedAt = timestamp;
                        attendance.updatedAt = timestamp;
                    }
                    realm.commitTransaction();
                } catch (JSONException | ParseException e) {
                    realm.cancelTransaction();
                    e.printStackTrace();
                    subscriber.onError(e);
                }
                subscriber.onNext(null);
                subscriber.onCompleted();
            }
        }));
    }

    static public Observable<Void> $getInouts(Realm realm, int gameId, int teamId) {
        return ApiClient.$getInouts(gameId, teamId)
                .map(response -> {
                    realm.beginTransaction();
                    realm.where(InOut.class).findAll().deleteAllFromRealm();
                    long timestamp = System.currentTimeMillis();

                    try {
                        for(int i = 0 ; i < response.length(); i++) {
                            JSONObject object = response.getJSONObject(i);
                            int quarter = object.getInt("quarter");
                            int playerId = object.getInt("memberId");
                            int inoutPoint = object.getInt("inout");

                            InOut inout = realm.createObject(InOut.class);
                            inout.game = realm.where(Game.class).equalTo("id", gameId).findFirst();
                            inout.team = realm.where(Team.class).equalTo("id", teamId).findFirst();
                            inout.player = realm.where(Player.class).equalTo("id", playerId).findFirst();
                            inout.quarter = quarter;
                            inout.inout = inoutPoint;
                            inout.syncedAt = timestamp;
                            inout.updatedAt = timestamp;
                        }
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        realm.cancelTransaction();
                        e.printStackTrace();
                        throw new Error(e.getMessage());
                    }

                    return null;
                });
    }

    static public Observable<Void> $getGames(Realm realm, int seasonId) {
        Map<Integer, Team> teamMap = new HashMap<>();
        RealmResults<Team> teams = realm.where(Team.class).findAll();

        for(Team team: teams) {
            teamMap.put(team.id, team);
        }

        return Observable.create(subscriber -> ApiClient.getGames(seasonId, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try {
                    realm.beginTransaction();
                    realm.where(Game.class).findAll().deleteAllFromRealm();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject gameObj = response.getJSONObject(i);
                        Game game = realm.createObject(Game.class, gameObj.getInt("id"));
                        game.game = gameObj.getInt("game");
                        game.homeTeam = teamMap.get(gameObj.getInt("homeTeamId"));
                        game.awayTeam = teamMap.get(gameObj.getInt("awayTeamId"));
                        game.date = dateFormat.parse(gameObj.getString("date"));
                    }
                    realm.commitTransaction();
                } catch (JSONException e) {
                    e.printStackTrace();
                    realm.cancelTransaction();
                    subscriber.onError(e);
                } catch (ParseException e) {
                    realm.cancelTransaction();
                    e.printStackTrace();
                }

                subscriber.onNext(null);
                subscriber.onCompleted();
            }
        }));
    }

    static public Observable<Void> $getStats(Realm realm, int gameId, int teamId) {
        String[] statColumns = { "pt3", "pt2", "ft", "ast", "reb", "stl", "blk", "to", "pf" };

        return ApiClient.$getStats(gameId, teamId)
                .map(response -> {
                    realm.beginTransaction();
                    realm.where(Stat.class).findAll().deleteAllFromRealm();
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject statObj = response.getJSONObject(i);
                            Game game = realm.where(Game.class).equalTo("id", gameId).findFirst();
                            Team team = realm.where(Team.class).equalTo("id", teamId).findFirst();
                            Player player = null;
                            if (statObj.has("memberId")) {
                                player = realm.where(Player.class).equalTo("id", statObj.getInt("memberId")).findFirst();
                            }
                            int quarter = statObj.getInt("quarter");

                            for (String column: statColumns) {
                                if (!statObj.has(column)) continue;

                                Stat stat = realm.createObject(Stat.class);
                                stat.game = game;
                                stat.team = team;
                                stat.player = player;
                                stat.quarter = quarter;
                                stat.point = statObj.getInt(column);
                                stat.statType = StatType.getFromString(column).id;
                            }
                        }
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        realm.cancelTransaction();
                        throw new Error("PARSE ERROR");
                    }

                    return null;
                });
    }

    public interface OnFinished {
        void onFinish();
    }
}
