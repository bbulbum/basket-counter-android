package me.kyubum.basketcounter.api;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import io.realm.RealmResults;
import io.realm.Sort;
import me.kyubum.basketcounter.models.Attendance;
import me.kyubum.basketcounter.models.Game;
import me.kyubum.basketcounter.models.InOut;
import me.kyubum.basketcounter.models.Player;
import me.kyubum.basketcounter.models.Season;
import me.kyubum.basketcounter.models.Stat;
import me.kyubum.basketcounter.models.StatType;
import me.kyubum.basketcounter.models.Team;
import me.kyubum.basketcounter.utils.BsktRealm;
import rx.Observable;
import rx.Subscriber;

public class ApiClient {
    static public SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

//    public static final String HOST_URL = "http://172.30.1.52:3000";
    public static final String HOST_URL = "http://basketapi.kyubum.me";

    static final AsyncHttpClient client = new AsyncHttpClient();

    public static void getSeasons(JsonHttpResponseHandler handler) {
        client.get(HOST_URL + "/seasons", handler);
    }

    public static void getGames(int seasonId, JsonHttpResponseHandler handler) {
        client.get(HOST_URL + "/seasons/" + seasonId + "/games", handler);
    }

    public static void getTeamMembers(int seasonId, JsonHttpResponseHandler handler) {
        client.get(HOST_URL + "/seasons/" + seasonId + "/teamMembers", handler);
    }

    public static void getAttendances(String gameDate, JsonHttpResponseHandler handler) {
        client.get(HOST_URL + "/attendances/" + gameDate, handler);
    }

    public static Observable<JSONArray> $getFreeThrows(String gameDate) {
        return Observable.create(subscriber -> {
           client.get(HOST_URL + "/freethrows/" + gameDate, new JsonHttpResponseHandler() {
               @Override
               public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                   subscriber.onNext(response);
                   subscriber.onCompleted();
               }

               @Override
               public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                   subscriber.onError(throwable);
               }
           });
        });
    }

    public static Observable<JSONArray> $getInouts(int gameId, int teamId) {
        return Observable.create(subscriber -> {
            client.get(HOST_URL + "/games/" + gameId + "/inouts/" + teamId, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    subscriber.onNext(response);
                    subscriber.onCompleted();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    subscriber.onError(throwable);
                }
            });
        });
    }

    public static Observable<JSONArray> $getStats(int gameId, int teamId) {
        return Observable.create(subscriber -> {
            client.get(HOST_URL + "/games/" + gameId + "/stats/" + teamId, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    subscriber.onNext(response);
                    subscriber.onCompleted();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    subscriber.onError(throwable);
                }
            });
        });
    }

    public static void postStat(Game game, Team team, Player player, int quarter, JsonHttpResponseHandler handler) throws Exception {
        if (!game.isValid() || !team.isValid() || !player.isValid()) return;

        RequestParams params = new RequestParams();
        params.put("teamId", team.id);
        params.put("memberId", player.id);
        params.put("quarter", quarter);

        int pt3 = 0, pt2 = 0, ft = 0, ast = 0, reb = 0, stl = 0, blk = 0, to = 0, pf = 0;

        RealmResults<Stat> stats = BsktRealm.getInstance().where(Stat.class)
                .equalTo("game.id", game.id)
                .equalTo("team.id", team.id)
                .equalTo("player.id", player.id)
                .equalTo("quarter", quarter)
                .findAllSorted("createdAt", Sort.ASCENDING);

        if (stats.size() > 0) {
            params.put("statCreatedAt", stats.last().createdAt);

            for (Stat stat: stats) {
                StatType statType = StatType.getFromId(stat.statType);
                if (statType == null) {
                    throw new Exception();
                }

                switch (statType) {
                    case PT3:
                        pt3 += stat.point;
                        break;
                    case PT2:
                        pt2 += stat.point;
                        break;
                    case FT:
                        ft += stat.point;
                        break;
                    case AST:
                        ast += stat.point;
                        break;
                    case REB:
                        reb += stat.point;
                        break;
                    case STL:
                        stl += stat.point;
                        break;
                    case BLK:
                        blk += stat.point;
                        break;
                    case TO:
                        to += stat.point;
                        break;
                    case PF:
                        pf += stat.point;
                        break;
                }
            }

            params.put("pt3", pt3);
            params.put("pt2", pt2);
            params.put("ft", ft);
            params.put("ast", ast);
            params.put("reb", reb);
            params.put("stl", stl);
            params.put("blk", blk);
            params.put("to", to);
            params.put("pf", pf);
        }

        client.post(HOST_URL + "/games/" + game.id + "/stats", params, handler);
    }

    static public void postFreethrow(Game game, Team team, int playerId, int point, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams();
        params.put("teamId", team.id);
        params.put("memberId", playerId);
        params.put("quarter", -1);
        params.put("ft", point);
        params.put("statCreatedAt", System.currentTimeMillis());

        client.post(HOST_URL + "/games/" + game.id + "/stats", params, handler);
    }

    static public void postAttendance(Season season, Attendance attendance, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams();
        params.put("seasonId", season.id);
        params.put("gameDate", dateFormat.format(attendance.date));
        params.put("teamId", attendance.team.id);
        params.put("memberId", attendance.player.id);
        params.put("point", attendance.point);
        params.put("isLeft", attendance.isLeft);

        client.post(HOST_URL + "/attendances", params, handler);
    }

    static public void postInout(InOut inout, JsonHttpResponseHandler handler) {
        RequestParams params = new RequestParams();
        params.put("quarter", inout.quarter);
        params.put("teamId", inout.team.id);
        params.put("memberId", inout.player.id);
        params.put("inout", inout.inout);

        client.post(HOST_URL + "/games/" + inout.game.id + "/inouts", params, handler);
    }

    static public Observable<Integer> $getGameAdvantage(int gameId, int teamId) {
        RequestParams params = new RequestParams();
        params.put("teamId", teamId);

        return Observable.create(subscriber -> client.get(HOST_URL + "/games/" + gameId + "/advantage", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    subscriber.onNext(response.getInt("advantage"));
                    subscriber.onCompleted();
                } catch (JSONException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }
            }
        }));
    }

    static public Observable<Void> $postGameAdvantage(int gameId, int teamId, int count) {
        RequestParams params = new RequestParams();
        params.put("teamId", teamId);
        params.put("count", count);

        return Observable.create(subscriber -> client.post(HOST_URL + "/games/" + gameId + "/advantage", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                subscriber.onNext(null);
                subscriber.onCompleted();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                subscriber.onError(throwable);
            }
        }));
    }
}
